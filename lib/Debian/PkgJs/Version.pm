package Debian::PkgJs::Version;

use Exporter 'import';

our $VERSION = '0.15.22';
our @EXPORT = qw($VERSION);

